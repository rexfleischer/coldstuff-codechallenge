package coldstuff;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.UUID;
import java.util.function.Supplier;

// the reason we should use a parameterized test suite
// is to ensure the test is consistent across all implementations
// of SmartFridgeManager. even though we only have one
// right now :)
@RunWith(Parameterized.class)
public class SmartFridgeManagerTest {

  @Parameterized.Parameters
  public static Object[][] data() {
    return new Object[][]{
        // if you have other implementations, add a supplier to this list
        new Object[]{(Supplier<SmartFridgeManager>) SmartFridgeManagerImpl::new}
    };
  }

  private final SmartFridgeManager manager;

  public SmartFridgeManagerTest(Supplier<SmartFridgeManager> factory) {
    this.manager = factory.get();
  }

  @Test
  public void testEmptyStateForgetItem() {
    // make sure it doesnt error if the item is not found
    manager.forgetItem(123);
  }

  @Test
  public void testEmptyStateGetFillFactor() {
    Assert.assertNull(manager.getFillFactor(123));
  }

  @Test
  public void testEmptyStateGetItems() {
    Assert.assertArrayEquals(new Object[0], manager.getItems(1.0));
  }

  @Test
  public void testEmptyStateHandleItemRemoved() {
    manager.handleItemRemoved("something-not-found");
  }

  @Test
  public void testEmptyStateHandleItemAdded() {
    manager.handleItemAdded(123, UUID.randomUUID().toString(), "item-name", 0.50);
  }

  @Test
  public void testFillFactorUpdatesOnRemoved() {
    String will_remove = UUID.randomUUID().toString();
    manager.handleItemAdded(123, will_remove, "item-name", 0.25);
    Assert.assertEquals(0.25, manager.getFillFactor(123), 0.000001);

    manager.handleItemAdded(123, UUID.randomUUID().toString(), "item-name", 0.50);
    Assert.assertEquals(0.50, manager.getFillFactor(123), 0.000001);

    manager.handleItemRemoved("something-not-found");
    Assert.assertEquals(0.50, manager.getFillFactor(123), 0.000001);

    manager.handleItemRemoved(will_remove);
    Assert.assertEquals(0.25, manager.getFillFactor(123), 0.000001);
  }

  @Test
  public void testGetItems() {
    manager.handleItemAdded(1231, UUID.randomUUID().toString(), "item-name-01", 0.10);
    manager.handleItemAdded(1232, UUID.randomUUID().toString(), "item-name-02", 0.20);
    manager.handleItemAdded(1233, UUID.randomUUID().toString(), "item-name-03", 0.30);
    manager.handleItemAdded(1234, UUID.randomUUID().toString(), "item-name-04", 0.40);
    manager.handleItemAdded(1235, UUID.randomUUID().toString(), "item-name-05", 0.50);
    manager.handleItemAdded(1236, UUID.randomUUID().toString(), "item-name-06", 0.60);
    manager.handleItemAdded(1237, UUID.randomUUID().toString(), "item-name-07", 0.70);
    manager.handleItemAdded(1238, UUID.randomUUID().toString(), "item-name-08", 0.80);
    manager.handleItemAdded(1239, UUID.randomUUID().toString(), "item-name-09", 0.90);
    manager.handleItemAdded(1230, UUID.randomUUID().toString(), "item-name-10", 1.00);

    Assert.assertArrayEquals(
        new Object[0],
        manager.getItems(0.05));
    Assert.assertArrayEquals(
        new Object[]{
            new Object[]{1231L, 0.10}},
        manager.getItems(0.10));
    Assert.assertArrayEquals(
        new Object[]{
            new Object[]{1231L, 0.10},
            new Object[]{1232L, 0.20},
            new Object[]{1233L, 0.30},
            new Object[]{1234L, 0.40}},
        manager.getItems(0.45));
  }
}

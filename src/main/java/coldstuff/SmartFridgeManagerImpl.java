package coldstuff;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class SmartFridgeManagerImpl implements SmartFridgeManager {
  /**
   * the basic bookkeeping object for a given item
   */
  private class ItemTypeInfo {
    private final long itemType;
    private final String name;
    private final Set<String> itemUUIDs = new LinkedHashSet<>();
    private double fillFactor = 1.0;
    private int desiredCount = 0;

    private ItemTypeInfo(long itemType, String name) {
      this.itemType = itemType;
      this.name = name;
    }

    /**
     * updates the desired count in a safe way based on itemUUIDs and fillFactor.
     */
    private void updateDesiredCount() {
      if (fillFactor <= 0.0) return;
      desiredCount = (int) ((double) itemUUIDs.size() / fillFactor);
    }

    /**
     * updates the fill factor in a safe way based on itemUUIDs and desiredCount.
     */
    private void updateFillFactor() {
      if (desiredCount <= 0) return;
      fillFactor = ((double) itemUUIDs.size() / (double) desiredCount);
    }
  }

  /**
   * a map of all items indexed by the itemType
   */
  private final Map<Long, ItemTypeInfo> items = new LinkedHashMap<>();

  @Override
  public void handleItemRemoved(String itemUUID) {
    // find the item or return.
    // NOTE: this could be optimized, but being that there isnt
    //       going to be 10Ks of objects in the fridge, this should
    //       be good enough. we did make it linked map though. so
    //       iteration should be pretty quick anyways.
    ItemTypeInfo item = items.values().stream()
        .filter(check -> check.itemUUIDs.contains(itemUUID))
        .findAny().orElse(null);
    if (item == null) return;

    // make sure to update the fill factor after we remote the item.
    item.itemUUIDs.remove(itemUUID);
    item.updateFillFactor();
  }

  @Override
  public void handleItemAdded(long itemType, String itemUUID, String name, Double fillFactor) {
    items.compute(itemType, (it, check) -> {
      ItemTypeInfo info = check == null ? new ItemTypeInfo(itemType, name) : check;
      info.itemUUIDs.add(itemUUID);
      info.fillFactor = fillFactor;
      // make sure to update the desired count with the given information.
      // we need to do this so we can update the fill factor for when an
      // item is removed.
      info.updateDesiredCount();
      return info;
    });
  }

  @Override
  public Object[] getItems(Double fillFactor) {
    return items.values().stream()
        .filter(check -> check.fillFactor <= fillFactor)
        .map(item -> new Object[] { item.itemType, item.fillFactor })
        .toArray();
  }

  @Override
  public Double getFillFactor(long itemType) {
    ItemTypeInfo check = items.get(itemType);
    return check != null ? check.fillFactor : null;
  }

  @Override
  public void forgetItem(long itemType) {
    items.remove(itemType);
  }
}
